import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TortureComponent } from './torture.component';

describe('TortureComponent', () => {
  let component: TortureComponent;
  let fixture: ComponentFixture<TortureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TortureComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TortureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
