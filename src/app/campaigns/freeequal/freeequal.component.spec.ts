import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeequalComponent } from './freeequal.component';

describe('FreeequalComponent', () => {
  let component: FreeequalComponent;
  let fixture: ComponentFixture<FreeequalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeequalComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeequalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
