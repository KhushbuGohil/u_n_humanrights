import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CampaignsPage } from './campaigns.page';
import { StandupComponent } from './standup/standup.component';
import { TortureComponent } from './torture/torture.component';
import { FreeequalComponent } from './freeequal/freeequal.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignsPage,
    children: [
      {
        path: '',
        redirectTo: 'standup',
        pathMatch: 'full',
      },
      {
        path: 'standup',
        component: StandupComponent,
      },
      {
        path: 'torture',
        component: TortureComponent,
      },
      {
        path: 'freeequal',
        component: FreeequalComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CampaignsPage, FreeequalComponent, TortureComponent, StandupComponent]
})
export class CampaignsPageModule {}
