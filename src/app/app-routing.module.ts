import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DonateComponent } from './donate/donate.component';
import { SettingsComponent } from './settings/settings.component';
import { StandupComponent } from './campaigns/standup/standup.component';
import { TortureComponent } from './campaigns/torture/torture.component';
import { FreeequalComponent } from './campaigns/freeequal/freeequal.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'TopStories',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'news', loadChildren: './news/news.module#NewsPageModule' },
  { path: 'campaigns', loadChildren: './campaigns/campaigns.module#CampaignsPageModule' },
  { path: 'yourrights', loadChildren: './yourrights/yourrights.module#YourrightsPageModule' },
  { path: 'quiz', loadChildren: './quiz/quiz.module#QuizPageModule' },
  { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  { path: 'donate', component: DonateComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'countries', loadChildren: './settings/countries/countries.module#CountriesPageModule' },
  { path: 'themes', loadChildren: './settings/themes/themes.module#ThemesPageModule' },
  { path: 'latest', loadChildren: './home/latest/latest.module#LatestPageModule' },
  { path: 'bycountry', loadChildren: './home/bycountry/bycountry.module#BycountryPageModule' },
  { path: 'bytheme', loadChildren: './home/bytheme/bytheme.module#BythemePageModule' },
  { path: 'regions', loadChildren: './home/bycountry/regions/regions.module#RegionsPageModule' },
  { path: 'topstories', loadChildren: './home/topstories/topstories.module#TopstoriesPageModule' },
  { path: 'weare', loadChildren: './about/weare/weare.module#WearePageModule' },
  { path: 'universal', loadChildren: './yourrights/universal/universal.module#UniversalPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
