import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private router: Router, public loadingController: LoadingController) { }

  ngOnInit()
  {
    this.presentLoading();
    setTimeout(() =>
    {
      this.router.navigate(['/TopStories']);
    }, 10000);
  }

  async presentLoading()
  {
    const loading = await this.loadingController.create({
      message: 'Loading...',
      duration: 3000,
      spinner: 'lines',
      cssClass: 'custom-class custom-loading'
    });
    await loading.present();
  }
}
