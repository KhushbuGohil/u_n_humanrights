import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AboutPage } from './about.page';
import { WearePage } from './weare/weare.page';
import { WedoComponent } from './wedo/wedo.component';

const routes: Routes = [
  {
    path: '',
    component: AboutPage,
    children: [
      {
        path: '',
        redirectTo: 'weare',
        pathMatch: 'full',
      },
      {
        path: 'weare',
        loadChildren: '../about/weare/weare.module#WearePageModule'
      },
      {
        path: 'wedo',
        component: WedoComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AboutPage, WedoComponent]
})
export class AboutPageModule {}
