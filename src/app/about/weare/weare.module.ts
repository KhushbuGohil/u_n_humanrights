import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WearePage } from './weare.page';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  {
    path: '',
    component: WearePage
  },
  {
    path: 'about',
    component: AboutComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WearePage, AboutComponent]
})
export class WearePageModule {}
