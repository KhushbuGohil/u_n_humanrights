import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopstoriesPage } from './topstories.page';

describe('TopstoriesPage', () => {
  let component: TopstoriesPage;
  let fixture: ComponentFixture<TopstoriesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopstoriesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopstoriesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
