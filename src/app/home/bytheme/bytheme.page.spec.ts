import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BythemePage } from './bytheme.page';

describe('BythemePage', () => {
  let component: BythemePage;
  let fixture: ComponentFixture<BythemePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BythemePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BythemePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
