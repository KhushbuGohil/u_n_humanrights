import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
        children: [
          {
            path: '',
            redirectTo: 'latest',
            pathMatch: 'full',
          },
          {
            path: 'latest',
            children: [
              {
                path: '',
                loadChildren: './home/latest/latest.module#LatestPageModule'
              }
            ]
          },
          {
            path: 'bycountry',
            children: [
              {
                path: '',
                loadChildren: './home/bycountry/bycountry.module#BycountryPageModule'
              }
            ]
          },
          {
            path: 'bytheme',
            children: [
              {
                path: '',
                loadChildren: './home/bytheme/bytheme.module#BythemePageModule'
              }
            ]
          }
        ]
      }
    ])
  ],
  declarations: [HomePage],
})
export class HomePageModule {}
