import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BycountryPage } from './bycountry.page';

describe('BycountryPage', () => {
  let component: BycountryPage;
  let fixture: ComponentFixture<BycountryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BycountryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BycountryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
