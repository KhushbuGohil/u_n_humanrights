import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit
{
  isLast = false;
  @ViewChild(IonSlides, {static: false }) slides: IonSlides;
  sliderOpts = {
    zoom: false,
    initialSlide: 1,
    // slidesPerView: 1.2,
    centeredSlides: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    allowTouchMove: true
  };
    
  constructor(public router: Router) { }
  ngOnInit()
  {
    setTimeout(() => {
      this.slides.lockSwipes(true);
    }, 200);
    
  }
  
  swipeNext()
  {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.getActiveIndex().then(index =>
    {
      if (index == 10)
      {
        this.isLast = true;
        
       }
    });
    this.slides.lockSwipes(true);
  }
  navigateTo()
  {
    this.slides.slideTo(0);
    this.isLast = false;
  }
}
