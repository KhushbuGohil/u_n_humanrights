import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'welcome',
      url: '/welcome'
    },
    {
      title: 'Top stories',
      url: '/TopStories',
      icon: 'book'
    },
    {
      title: 'News',
      url: '/news',
      icon: 'paper'
    },
    {
      title: 'Campaigns',
      url: '/campaigns',
      icon: 'megaphone'
    },
    {
      title: 'Your Rights',
      url: '/YourRights',
      icon: 'person'
    },
    {
      title: 'About',
      url: '/about',
      icon: 'thumbs-up'
    },
    {
      title: 'Quiz',
      url: '/quiz',
      icon: 'stopwatch'
    },
    {
      title: 'Settings',
      url: '/settings',
      icon: 'settings'
    }
  ];
  language: any;
  constructor(
    private platform: Platform, public storage: Storage, private splashScreen: SplashScreen, private statusBar: StatusBar,
    private translate: TranslateService) {
    this.initializeApp();
  }

  initializeApp() {
    this.storage.get('selectedLang').then((lang) => {
      if (lang) {
        this.translate.use(lang);
        this.language = lang;
      } else {
        this.translate.use('en');
      }
    });
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
