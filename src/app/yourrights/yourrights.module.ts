import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { YourrightsPage } from './yourrights.page';
import { LearnComponent } from './learn/learn.component';

const routes: Routes = [
  {
    path: '',
    component: YourrightsPage,
    children: [
      {
        path: '',
        redirectTo: 'learn',
        pathMatch: 'full',
      },
      {
        path: 'learn',
        component: LearnComponent,
        pathMatch: 'full',
      },
      {
        path: 'universal',
        loadChildren: '../yourrights/universal/universal.module#UniversalPageModule',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [YourrightsPage, LearnComponent]
})
export class YourrightsPageModule {}
