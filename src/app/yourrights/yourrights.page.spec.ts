import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourrightsPage } from './yourrights.page';

describe('YourrightsPage', () => {
  let component: YourrightsPage;
  let fixture: ComponentFixture<YourrightsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourrightsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourrightsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
