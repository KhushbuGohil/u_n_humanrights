import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  language: any;
  visibility: boolean;
  value: any;
  value1: any;
  
  constructor(public translate: TranslateService, public storage: Storage, public alertController: AlertController,
    public router: Router)
  {
    this.language = 'en';
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }

  btndisabled(e)
  {
    console.log(e);
    this.value = e.detail.value;
  }

  btndisabled1(e)
  {
    console.log(e);
    this.value1 = e.detail.value
  }

  async languageChange()
  {
    const alert = await this.alertController.create({
      header: 'Exit',
      message: 'This application will be restarted. Do you want to change language?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () =>
          {
            this.storage.get('selectedLang').then((lang) => {
              this.language = lang;
            });
            alert.dismiss(true);
          }
        }, {
          text: 'Okay',
          handler: () =>
          {
            this.router.navigate(['/welcome']);
            this.translate.use(this.language);
            this.storage.set('selectedLang', this.language);
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnInit() {}

}
